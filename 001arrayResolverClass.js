// Write a Solver class/object in Javascript that would accept an array in the
// form of:
// [3 , “Plus”, 1] , [6 , “Times”, 2]... or an object with properties “operand1”, “operator” and
// “operand2”, the option that you prefer, and returns the result of the arithmetic.
// Note: element representing the operator is the name of a function or a reference to that function –
// implement them as well (Plus, Minus, Times and Divide).
// Note: in the future there may be a need to support another operations without changing the
// existing code or changing it as little as possible.
// 001(a): Javascript - make your implementation recursive (so you can pass other operations
// instead of any or both operators, something like: [3 , “Plus”, [6, “Times”, 2]] ), that would
// be equivalent to
// 3 + (6 * 2).
class Solver {
    calculate([operand1, operator, operand2]) {
        if (Array.isArray(operand1)) {
            operand1 = new Solver().calculate(operand1);
        }
        if (Array.isArray(operand2)) {
            operand2 = new Solver().calculate(operand2);
        }
        switch (operator) {
            case "Plus":
                return this.plus(operand1, operand2);
            case "Minus":
                return this.minus(operand1, operand2);
            case "Times":
                return this.times(operand1, operand2);
            case "Divide":
                return this.divide(operand1, operand2);
            default:
                break;
        }
    }
    plus(operand1, operand2) {
        return operand1 + operand2;
    }
    minus(operand1, operand2) {
        return operand1 - operand2;
    }
    times(operand1, operand2) {
        return operand1 * operand2;
    }
    divide(operand1, operand2) {
        return operand1 / operand2;
    }
}
console.log(new Solver().calculate([3, "Plus", [
    [6, "Minus", 2], "Times", 2
]]));
console.log(new Solver().calculate([3, "Plus", [6, "Times", 2]]));
console.log(new Solver().calculate([3, "Plus", 6]));