# MD4S coding solution




## 001: Javascript

Write a Solver class/object in Javascript that would accept an array in the
form of:
[3 , “Plus”, 1] , [6 , “Times”, 2]... or an object with properties “operand1”, “operator” and
“operand2”, the option that you prefer, and returns the result of the arithmetic.
Note: element representing the operator is the name of a function or a reference to that function –
implement them as well (Plus, Minus, Times and Divide).
Note: in the future there may be a need to support another operations without changing the
existing code or changing it as little as possible.
001(a): Javascript - make your implementation recursive (so you can pass other operations
instead of any or both operators, something like: [3 , “Plus”, [6, “Times”, 2]] ), that would
be equivalent to
3 + (6 * 2).

---

## 002: MySql

Having the follow table structures: tbl_user: user_id, user_full_name, tbl_role:
role_id, role_name and tbl_user_role: user_id, role_id.
- Create the mysql code to create these tables.
- Write a query that will give you all users that belong to the role_name IT
- Write a query that will give you a list of users with no roles
- (Bonus) Write a query that will find the users with duplicate roles.

---

## 003: Html & CSS 

 Create a small gallery with some images of your choice. The images can
be stored in a static list (so you can just use Javascript & HTML, and if you want some CSS, but
don’t worry about the look and feel). Gallery doesn’t have to be of type “slider”, it should be
possible to go to the next and previous image (it doesn’t matter if it’s using arrows, clicking in a
part of the image...), but Gallery MUST not auto-advance.
We’d like to track each image impression in our own or a third-party real-time analytics system
(you can use any invented url, just make sure that some url with at least an image identifier is
called for each image impression).
That analytics system is sitting on a completely different domain (for example
http://dev.test06/index.php) and for the purpose of that exercise is very basic:
```php
<?php
$log = "GET:".print_r($_GET,1)."POST:".print_r($_POST,1);
error_log( $log, 3, 'log.log');
?>
```
## 004: PHP/Python
 The Canadian National Lottery draw takes place
twice per week, on Tuesday and Sunday at 9.30 pm.
Write a class in PHP and Python that returns the next valid draw date based on the current date.
Make your code able to calculate next draw based on an optionally supplied date and time.
*Note: if you can write it in both languages, else do it in the one that you can

---
