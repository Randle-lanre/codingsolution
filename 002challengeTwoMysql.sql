-- MySql - Having the follow table structures: tbl_user: user_id, user_full_name, tbl_role:
-- role_id, role_name and tbl_user_role: user_id, role_id.
-- - Create the mysql code to create these tables.
-- - Write a query that will give you all users that belong to the role_name IT
-- - Write a query that will give you a list of users with no roles
-- - (Bonus) Write a query that will find the users with duplicate roles.




-- Question1: Create the mysql code to create these tables.


-- table structure 


-- tbl_user:      
--    user_id, user_full_name
-- tbl_role:
--     role_id, role_name 
-- tbl_user_role:  
--     user_id, role_id.  

DROP TABLE IF EXISTS tbl_user;
CREATE TABLE tbl_user (
  user_id INT NOT NULL AUTO_INCREMENT,
  user_full_name VARCHAR(100) NOT NULL,
  PRIMARY KEY (user_id)
);
DROP TABLE IF EXISTS tbl_role;
CREATE TABLE tbl_role(
  role_id INT NOT NULL AUTO_INCREMENT,
  role_name VARCHAR(100),
  PRIMARY KEY(role_id)
);
DROP TABLE IF EXISTS tbl_user_role;
CREATE TABLE tbl_user_role(
  user_id INT,
  role_id INT,
  FOREIGN KEY (user_id) REFERENCES tbl_user(user_id) ,
  FOREIGN key (role_id) REFERENCES tbl_role(role_id) 
);









-- Question2: Write a query that will give you all users that belong to the role_name IT

SELECT
  tbl_user.user_full_name,
  tbl_role.role_name
from
  tbl_user_role
  join tbl_user on tbl_user.user_id = tbl_user_role.user_id
  join tbl_role on tbl_role.role_id = tbl_user_role.role_id
where
  tbl_role.role_name = "IT";



-- Question 3 Write a query that will give you a list of users with no roles
select
  tbl_user.user_full_name
from
  tbl_user_role
  join tbl_user on tbl_user.user_id = tbl_user_role.user_id
  join tbl_role on tbl_role.role_id = tbl_user_role.role_id
where
  tbl_role.role_name = NULL;



-- (Bonus) Write a query that will find the users with duplicate roles.

select 
  tbl_user.user_full_name, count(tbl_role.role_id)
from 
  tbl_user_role
  join tbl_user on tbl_user.user_id = tbl_user_role.user_id
  join tbl_role on tbl_role.role_id = tbl_user_role.role_id
GROUP BY tbl_role.role_id
HAVING count(tbl_role.role_id) > 1;
  






