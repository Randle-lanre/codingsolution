import datetime

'''
: PHP/Python* - The Canadian National Lottery draw takes place
twice per week, on Tuesday and Sunday at 9.30 pm.
Write a class in PHP and Python that returns the next valid draw date based on the current date.
Make your code able to calculate next draw based on an optionally supplied date and time.

'''

class NextLotteryDate:
  
#set givenDate and timeGiven as optional argument
#incase a date is not provided, the current date would  be used
#and time can be provided or not
  def nextDate(self, givenDate=None, timeGiven=None):
    #check that a date is given
    if givenDate==None:
        #if date is not given use today's date
      givenDate=datetime.date.today()
    else:
        #otherwise use the date given
      givenDate=datetime.date.fromisoformat(givenDate)
  
  #update the date value (if a date was given or not)
    parsedDate=givenDate
   
   #if a time was given get that as well and check its not greater than 9:30
    if (timeGiven)!=None:
      if ((datetime.time(timeGiven[0],timeGiven[1]))>(datetime.time(21, 30))):
          #if the time is greater than 9:30 update the day of the week to the nextday
        parsedDate.weekday()+1 

    if (parsedDate.weekday()>1):
      while parsedDate.weekday() !=6 :
        parsedDate+= datetime.timedelta(1)
    
    elif(parsedDate.weekday()==0):
      while parsedDate.weekday()!=1:
        parsedDate+= datetime.timedelta(1)
    else:
      print('something went wrong')

    
    return parsedDate









test= NextLotteryDate()
#no arguments passed, uses todays date
checkresult=test.nextDate()  
print(checkresult)

#uses an optional date paremter
checkresult=test.nextDate('2020-01-15')
print(checkresult)


#uses both date and tim(24 hrs)
#here 23 is the hour 
# 10 is the minute
checkresult=test.nextDate('2020-01-15',[23,10])  

print(checkresult)